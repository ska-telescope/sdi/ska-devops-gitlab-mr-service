# Python Web Server Template

This project is the template projects that is intended for use by SKA DevSecOps Service Applications (_mainly bots_).
The project and generated projects from this template should follow the [SKA Developer Portal](https://developer.skatelescope.org/en/latest/) closely.
Please check the latest guidelines, standards and practices before moving forward!

## Set up

Install [Poetry](https://python-poetry.org/) for Python package and environment management. This project is structured to use this tool for dependency management and publishing.

However if you want to use other tools (`Pipenv` or any other tool that can work with `requirements.file`) run the following script to generate a requirements file.

```console
pip install Poetry
make exportlock
```

This will generate `requirements.txt` and `requirements-dev.txt` files for  runtime and development environment.

## Structure

Project structure is following a basic python file structure for [FastAPI](https://fastapi.tiangolo.com/) as below:

```console
.
├── Dockerfile
├── LICENSE
├── Makefile
├── README.md
├── app
│   ├── ...
├── build
│   ├── ...
├── charts
│   ├── ...
├── conftest.py
├── docs
├── pyproject.toml
└── tests
    ├── ...
```

Basically, this project uses the following technologies:

- Docker: To build a docker image that exposes port _80_ for API endpoints.
- Kubernetes and Helm: Project also includes a helm chart to deploy the above image in a loadbalanced kubernetes cluster.
- Python Package: Project also includes a python package so that it can be downloaded as such. (**The usability of this capability highly depends on the actual implementation!**)

## Local Development

### General Workflow

By default, dependencies are managed with [Poetry](https://python-poetry.org/), either install it from the website or using below command:

```console
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

#### What is Poetry and Why are we using it

**Poetry** is a python dependency management and packaging tool. It is similar to [pipenv](https://pypi.org/project/pipenv/) for dependency management but it is more actively developed. It organizes depedencies in seperate sections of the same file using `pyproject.toml`, described in [PEP 518](https://www.python.org/dev/peps/pep-0518/) so it can specify publishing information and configure installed tools (like black, isort, tox etc.) which makes it easy to both configure and manage dependencies and publishing.

Then, you can install all the dependencies with:

```console
make requirements
```

Now, the project is ready for local development.

**Note:** depending on the IDE (_vscode is suggested_), `PYTHONPATH` may need to be adjusted so that IDE picks up imports and tests correctly.
Please refrain from changing the main structure (_top level folders_) as it may break the CI/CD pipeline, make targets and the very fabric of the universe may be at stake.

### Linting and code-style

The linting step uses `black, flake8, isort and pylint` tools to check to code. It maybe useful to adjust your local environment as such as well.

Run `make lint` to check your code to see any linting errors. `make apply-formatting` could also be used to auto adjust the code style with `black` and `isort`.
Note this also includes tests as well.

### Building and Running

*k8s note*: if you are deploying or testing locally using Minikube, you should first run `eval $(minikube docker-env)` before you create your images, so that Minikube will pull the images directly from your environment.

To build the project for local development (and releasing in later stages), run `make build`.
This will build a docker image (if a tag is present it will also tag it accordingly, or a _dirty_ tag will be used) and will build the python package as well.

To run/deploy the project, you can use Docker and Kubernetes as described below.

#### With Docker

Testing with Docker only is also possible: `make development` starts the latest built docker image (`make docker-build`) with app folder mounted into it and the server is set to `--reload` flag. This enables local development by reflecting any change in your app/ folder to loaded into the api server as well.

#### With Kubernetes

An example minikube installation with loadbalancer enabled could be found [here](https://gitlab.com/ska-telescope/sdi/deploy-minikube/) - this is the suggested starting point for testing locally with Minikube.

You want to install charts using the docker image created with `make docker-build`. If you ran `eval $(minikube docker-env)` before building, the image will be pulled from your local cache.

Next, you want to deploy your charts. `make install-chart` deploys the helm chart into a k8s environment with the following ingress controllers:

- NGINX
- Traefik

By default, it uses `traefik` for local development and testing. You can override this by providing `INGRESS` variable like `make install-chart INGRESS=nginx`. 
In deployment correct ingress is automatically selected.

### Testing With FastAPI and Pytest-BDD

The tests are done with pytest-bdd style, and are located on the testing folder on a server.feature and a test_server.py.
To add new tests edit only server.feature file.

BDD style tests are created that test the correct functioning of:

- Check if Main route gives the Right response
- Post using a .json file (Get cannot be done with body )
- Get using a target (Post can be done only with target aswell)

Because FastAPI is being used, tests are done by using the FastAPI TestClient - you can read more about it [here](https://fastapi.tiangolo.com/tutorial/testing/).

To run tests:

```console
$ make unit_test
                                    .
                                    .
                                    .
platform linux -- Python 3.6.9, pytest-6.1.2, py-1.9.0, pluggy-0.13.1
rootdir: /home/clean/ska-devops-gitlab-mr-service/testing, configfile: pytest.ini
plugins: bdd-4.0.1
collected 3 items

test_server.py::test_check_server PASSED                                                     [ 33%]
test_server.py::test_add_member_with_json PASSED                                             [ 66%]
test_server.py::test_get_member_id PASSED                                                    [100%]
```

### Publishing/Releasing

All the publishing should happen from the pipeline.

*TL;DR: run `make release` to learn what you have to do!*

When you are ready to publish a new version, you need to run `make update-x-release` where `x` is either `patch`, `minor` or `major`.
So if you want to update the patch version, you just run `make update-patch-release`.

This will update the necessary version labels in `.release` (for docker image) , `pyproject.toml` (for python package) files and will make a commit and tag it accordingly. At this stage, if you can use `make push` to manually push the docker image to your configured registry although it is not encouraged.

Finally, run `make release`.
Once the CI job has completed in the [pipeline](https://gitlab.com/ska-telescope/sdi/ska-devops-gitlab-mr-service/pipelines),
make sure you trigger the manual step on the tag ref for publishing either for docker/python or deploying the helm chart.
