import logging

from ska.gitlab_api import GitLabApi


class AddCommentExecutor:
    def __init__(self, logger_name):
        self.api = GitLabApi()
        self.logger = logging.getLogger(logger_name)

    async def action(self, proj_id, mr_id, comment):
        await self.api.authenticate()
        result = await self.api.add_comment(proj_id, mr_id, comment)
        self.logger.debug("Command result: %s", result)
        await self.api.close_session()
        return result
