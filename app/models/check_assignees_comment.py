import logging

from ska.gitlab_api import GitLabApi


class CheckAssigneesComment:
    def __init__(self, logger_name):
        self.api = GitLabApi()
        self.logger = logging.getLogger(logger_name)

    async def check(self, proj_id, mr_id):
        await self.api.authenticate()
        mr = await self.api.get_merge_request_info(proj_id, mr_id)
        self.logger.debug("Retrieved MR: %s", mr)
        await self.api.close_session()
        return len(mr["assignees"]) > 0

    async def text_comment(self):
        return "There must be at least one assignee for the MR"
