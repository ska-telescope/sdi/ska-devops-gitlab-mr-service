import logging

from ska.gitlab_api import GitLabApi


class CheckDelSourceBranchComment:
    def __init__(self, logger_name):
        self.api = GitLabApi()
        self.logger = logging.getLogger(logger_name)

    async def check(self, proj_id, mr_id):
        await self.api.authenticate()
        mr = await self.api.get_merge_request_info(proj_id, mr_id)
        self.logger.debug("Retrieved MR: %s", mr)
        await self.api.close_session()
        return mr["force_remove_source_branch"]

    async def text_comment(self):
        return "Please check 'Delete source branch when merge request is accepted.'"
