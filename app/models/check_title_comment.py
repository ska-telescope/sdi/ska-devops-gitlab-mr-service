import logging
import re

from ska.gitlab_api import GitLabApi
from ska.jira_api import SKAJira


class CheckTitleComment:
    def __init__(self, logger_name):
        self.api = GitLabApi()
        self.jira = SKAJira()
        self.logger = logging.getLogger(logger_name)

    async def check(self, proj_id, mr_id):
        await self.api.authenticate()
        await self.jira.authenticate()
        mr = await self.api.get_merge_request_info(proj_id, mr_id)
        await self.api.close_session()

        # self.logger.info("Retrieved MR: %s", mr)
        self.logger.info("MR Title: %s", mr["title"])

        issue_key_match = re.search(r"[A-Z]+-\d*", mr["title"])
        if not issue_key_match:
            self.logger.info(
                "Regular expression failed to get a Jira Issue in the MR Title string"
            )
            return False
        else:
            possible_issue_key = issue_key_match.group(0)
            self.logger.info("Possible Issue key found: %s", possible_issue_key)

        issue_obj = await self.jira.get_issue(possible_issue_key)
        self.logger.debug("Returned issue object: %s", issue_obj)

        return issue_obj is not None

    async def text_comment(self):
        return "Title should include a Jira ticket id"
