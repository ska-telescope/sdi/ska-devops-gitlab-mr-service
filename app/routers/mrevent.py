""" GitLab Route for managing MR web hooks """
import logging

from fastapi import APIRouter, Request

from app.models.add_comment_executor import AddCommentExecutor
from app.models.check_assignees_comment import CheckAssigneesComment
from app.models.check_del_source_bransh_comment import CheckDelSourceBranchComment
from app.models.check_do_not_squash_commits_comment import CheckDoNotSquashComment
from app.models.check_title_comment import CheckTitleComment

logger = logging.getLogger("gunicorn.error")

router = APIRouter()


@router.post("/")
async def new_event(request: Request):
    """Triggered when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch"""
    body_json = await request.json()
    mr_id = body_json["object_attributes"]["iid"]
    logger.info("Received mr_id: %s", mr_id)
    proj_id = body_json["project"]["id"]
    logger.info("Received proj_id: %s", proj_id)

    comment = "This is an automatic generated comment:\n\n"
    add_comment = False

    check = CheckTitleComment("gunicorn.error")
    if not await check.check(proj_id, mr_id):
        add_comment = True
        comment = comment + "- " + await check.text_comment() + "\n"

    check = CheckAssigneesComment("gunicorn.error")
    if not await check.check(proj_id, mr_id):
        add_comment = True
        comment = comment + "- " + await check.text_comment() + "\n"

    check = CheckDelSourceBranchComment("gunicorn.error")
    if not await check.check(proj_id, mr_id):
        add_comment = True
        comment = comment + "- " + await check.text_comment() + "\n"

    check = CheckDoNotSquashComment("gunicorn.error")
    if not await check.check(proj_id, mr_id):
        add_comment = True
        comment = comment + "- " + await check.text_comment() + "\n"

    if add_comment:
        logger.info("Adding comment: %s", comment)
        executor = AddCommentExecutor("gunicorn.error")
        result = await executor.action(proj_id, mr_id, comment)
        logger.info("Result add comment: %s", result)

    return 200
