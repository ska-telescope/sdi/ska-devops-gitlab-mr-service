# import json
# import logging

# import pytest
import requests
import json
import logging

def test_get():
    url = 'http://test-mr-service-ska-devops-gitlab-mr-service:80/'

    response = requests.get(url)
    assert response.status_code == 200

def test_post_events():
    url = 'http://test-mr-service-ska-devops-gitlab-mr-service:80/events/'

    with open("resources/event.json", "r") as myfile:
            data = myfile.read()

    obj = json.loads(data)

    response = requests.post(url, json=obj)
    assert response.status_code == 200

